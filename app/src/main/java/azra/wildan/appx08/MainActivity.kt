package azra.wildan.appx08

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "Setting"
    val RC_SUKSES: Int = 100

    //    variabel ganti warna
    var bg: String = ""
    var fontheader: String = ""
    val DEF_BACK = bg
    val DEF_BACK_FONT_HEAD = fontheader
    val FIELD_BACK = "BACKGROUND"
    val FIELD_FONTHEAD = "BACKGROUND_FONTHEADER"

    //    var ganti ukuran font
    var fSubTitle: Int = 30
    var fDetail: Int = 30
    val DEF_FONT_DETAIL = fDetail
    val DEF_FONT_SUBTITLE = fSubTitle
    val FIELD_FONT_DETAIL = "Font_Size_Detail"
    val FIELD_FONT_SUBTITLE = "Font_Size_SubTitle"

    //    var edit text
    var judul : String = "Hello World"
    var detail : String = "“Kota ini, aku, kamu, dan dunia yang aku kira nyata, ternyata semuanya tak lebih dari sekedar data”.  Bercerita pada tahun 2027 di Kyoto, Naomi Katagaki, seorang murid SMA bertemu dengan seorang laki-laki yang mengaku sebagai dirinya dari 10 tahun yang akan datang. Bersama-sama mereka harus merubah masa depan untuk menyelamatkan salah seorang murid perempuan yang akan ia cintai tiga bulan yang akan datang."
    val FIELD_TEXT = "text"
    val DEF_DETAIL = detail
    val DEF_JUDUL = judul

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.itemSet -> {
                var intent = Intent(this, SettingActivity::class.java)
                intent.putExtra("judul", txtTitle.text.toString())
                intent.putExtra("detail", txtDetail.text.toString())
                startActivityForResult(intent, RC_SUKSES)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txtDetail.setText(preferences.getString(FIELD_TEXT, DEF_DETAIL))
        txtTitle.setText(preferences.getString(FIELD_TEXT, DEF_JUDUL))
        fSubTitle = preferences.getInt(FIELD_FONT_SUBTITLE, DEF_FONT_SUBTITLE)
        fDetail = preferences.getInt(FIELD_FONT_DETAIL, DEF_FONT_DETAIL)
        bg = preferences.getString(FIELD_BACK, DEF_BACK).toString()
        fontheader = preferences.getString(FIELD_FONTHEAD, DEF_BACK_FONT_HEAD).toString()

        background()
        fontheadercolor()
        subtitle()
        detail()
    }

    fun background() {
        if (bg == "BLUE") {
            constraintlayout.setBackgroundColor(Color.BLUE)
        } else if (bg == "YELLOW") {
            constraintlayout.setBackgroundColor(Color.YELLOW)
        } else if (bg == "GREEN") {
            constraintlayout.setBackgroundColor(Color.GREEN)
        } else if (bg == "BLACK") {
            constraintlayout.setBackgroundColor(Color.BLACK)
        }
    }

    fun fontheadercolor() {
        if (fontheader == "Blue") {
            txtHeader.setTextColor(Color.BLUE)
        } else if (fontheader == "Yellow") {
            txtHeader.setTextColor(Color.YELLOW)
        } else if (fontheader == "Green") {
            txtHeader.setTextColor(Color.GREEN)
        } else if (fontheader == "Black") {
            txtHeader.setTextColor(Color.BLACK)
        } else {
            txtHeader.setTextColor(Color.WHITE)
        }
    }

    fun subtitle() {
        txtSubtitle.textSize = fSubTitle.toFloat()
    }

    fun detail() {
        txtDetail.textSize = fDetail.toFloat()
    }

    override fun onClick(v: View?) {
//        preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
//        val prefEditor = preferences.edit()
//        prefEditor.putString(FIELD_TEXT,edText.text.toString())
//        prefEditor.putInt(FIELD_FONT_SIZE,sbar.progress)
//        prefEditor.commit()
//        Toast.makeText(this, "Perubahan telah disimpan", Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RC_SUKSES)
                txtDetail.setText(data?.extras?.getString("isidetail"))
            txtTitle.setText(data?.extras?.getString("title"))
            bg = data?.extras?.getString("bgColor").toString()
            fontheader = data?.extras?.getString("fhColor").toString()
            fSubTitle = data?.extras?.getInt("tHead").toString().toInt()
            fDetail = data?.extras?.getInt("tTitle").toString().toInt()
            preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val prefEdit = preferences.edit()
            prefEdit.putString(FIELD_BACK,bg)
            prefEdit.putString(FIELD_FONTHEAD, fontheader)
            prefEdit.putInt(FIELD_FONT_SUBTITLE, fSubTitle)
            prefEdit.putInt(FIELD_FONT_DETAIL, fDetail)
            prefEdit.putString(FIELD_TEXT,detail)
            prefEdit.putString(FIELD_TEXT,judul)
            prefEdit.commit()

            background()
            fontheadercolor()
            subtitle()
            detail()


        }

    }
}
